module gitee.com/hyperledger-fabric-gm/cfssl

go 1.16

require (
	github.com/google/certificate-transparency-go v1.1.1
	github.com/jmhodges/clock v0.0.0-20160418191101-880ee4c33548
	github.com/jmoiron/sqlx v1.3.4
	github.com/kisielk/sqlstruct v0.0.0-20210630145711-dae28ed37023
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	golang.org/x/net v0.0.0-20210716203947-853a461950ff
)
